Source: python-model-bakery
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Neil Williams <codehelp@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-setuptools,
 python3-all,
 python3-django (>= 3.2),
 python3-sphinx <!nodoc>,
 python3-pytest (>= 7.1.2) <!nocheck>,
 python3-pytest-django (>= 4.5.2) <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://github.com/model-bakers/model_bakery
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-model-bakery
Vcs-Git: https://salsa.debian.org/python-team/packages/python-model-bakery.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-model-bakery
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-model-bakery-doc
Description: smart object creation facility for Django (Python 3 version)
 Model-Bakery is the replacement for model-mommy and offers you a smart way
 to create fixtures for testing in Django.  With a simple and powerful API
 you can create many objects with a single line of code.
 .
 This package installs the library for Python 3.

Package: python-model-bakery-doc
Architecture: all
Section: doc
Multi-Arch: foreign
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: smart object creation facility for Django (common documentation)
 Model-Bakery is the replacement for model-mommy and offers you a smart way
 to create fixtures for testing in Django.  With a simple and powerful API
 you can create many objects with a single line of code.
 .
 This is the common documentation package.
